const fs = require('fs');

function readFile(filePath) {
    const promise = new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });

    return promise;
}

function writeFile(fileName, data) {
    const promise = new Promise((resolve, reject) => {
        fs.writeFile(fileName, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(fileName);
            }
        });
    });

    return promise;
}

function writeFileNames(data) {
    const flags = {
        flag: 'a'
    };

    const promise = new Promise((resolve, reject) => {
        fs.writeFile('filenames.txt', `${data},`, flags, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });

    return promise;
}

function deleteFiles(fileNames) {
    const files = fileNames.split(',');
    const promise = new Promise((resolve, reject) => {
        files.filter((file) => {
            file.trim();
            return file !== '';
        })
            .map((file) => {
                fs.unlink(`./${file}`, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                })
            });
    });
}

function index() {
    readFile('../lipsum.txt').then((result) => {
        return writeFile('lipsumUpperCase.txt', result.toUpperCase());
    })
        .then((fileName) => {
            return writeFileNames(fileName);
        })
        .then(() => {
            return readFile('lipsumUpperCase.txt');
        })
        .then((result) => {
            const contentToLowerCase = result.toLowerCase();
            const splitContent = contentToLowerCase.split('.');
            return writeFile('lipsumLowerCase.txt', splitContent.join('~'));
        })
        .then((fileName) => {
            return writeFileNames(fileName);
        })
        .then(() => {
            return readFile('lipsumLowerCase.txt');
        })
        .then((result) => {
            const sortData = result.split('~')
                .map((content) => {
                    return content.trim();
                })
                .sort((content1, content2) => {
                    if (content1 > content2) {
                        return 1;
                    } else if (content2 > content1) {
                        return -1
                    } else {
                        return 0;
                    }
                });

            return writeFile('lipsumSorted.txt', sortData.join('\n'));
        })
        .then((fileName) => {
            return writeFileNames(fileName);
        })
        .then(() => {
            return readFile('filenames.txt');
        })
        .then((result) => {
            return deleteFiles(result);
        })
        .catch((err) => {
            console.error(err);
        });
}

module.exports = index;