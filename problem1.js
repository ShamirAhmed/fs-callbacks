const fs = require('fs');

function createDir(dirName) {
    const promises = new Promise((resolve, reject) => {
        if (fs.existsSync(dirName)) {
            resolve('Directory Exists');
        } else {
            fs.mkdir('./' + dirName, (err) => {
                if (err) {
                    reject(error);
                } else {
                    resolve('Directory Created')
                }
            });
        }
    });

    return promises;
}

function createRandomFiles(fileName) {
    const filePath = `./random/file${fileName}.json`;
    const promises = new Promise((resolve, reject) => {
        fs.writeFile(filePath, '', (error) => {
            if (error) {
                reject(error);
            } else {
                console.log(`file${fileName}.json created`)
                resolve();
            }
        })
    });

    if (fileName > 0) {
        createRandomFiles(fileName - 1);
    }

    return promises;
}

function deleteFiles(fileName) {
    const filePath = `./random/file${fileName}.json`;
    const promise = new Promise((resolve, reject) => {
        fs.unlink(filePath, (error) => {
            if (error) {
                reject(error);
            } else {
                resolve();
                console.log(`file${fileName}.json deleted`);
            }
        });
    });

    if (fileName > 0) {
        deleteFiles(fileName - 1);
    }

    return promise;
}

function randomFiles() {
    const fileName = parseInt(Math.random() * 10);

    createDir('random').then((result) => {
        console.log(result);
        return createRandomFiles(fileName);
    }).then(() => {
        return deleteFiles(fileName);
    }).catch((error) => {
        console.error(error);
    })
}

module.exports = randomFiles;